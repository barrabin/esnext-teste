# Enext Image Cropper

A HTML5 image cropper exercise for e-next.

### Installation

```
npm install
```

### Start Dev Server

```
npm run dev
```

### Build Prod Version

```
npm run build
```

### Features:
