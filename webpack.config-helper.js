'use strict';

const Path = require('path')
const Webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ExtractSASS = new ExtractTextPlugin('assets/styles/[name].css', { allChunks: true});

module.exports = (options) => {

  let webpackConfig = {
    devtool: options.devtool,
    entry: {
      'app': [
        `webpack-dev-server/client?http://localhost:${options.port}`,
        'webpack/hot/dev-server',
        './src/js/index'],
      'vendor': ['classie', 'modernizr', 'cropperjs','wolfy87-eventemitter','./src/js/utils']
    },
    output: {
      path: Path.join(__dirname, 'dist'),
      filename: 'assets/js/[name].js',
    },
    plugins: [
      new Webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify(options.isProduction ? 'production' : 'development')
        }
      }),
      new HtmlWebpackPlugin({
        template: './src/index.html'
      }),
      new Webpack.optimize.CommonsChunkPlugin("vendor", "assets/js/vendor.js", Infinity),
    ],
    module: {
      loaders: [{
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel',
        query: {
          presets: ['es2015']
        }
      },{
        test: /\.modernizrrc$/,
        loader: "modernizr"
      },
      { test: /\.css$/, loader: "style-loader!css-loader" },
      {
        //IMAGE LOADER
        test: /\.(jpe?g|png|gif|svg)$/i,
        loader:'file-loader?name=assets/images/[name].[ext]'
        // loader:'file-loader?name=assets/images/[name].[ext]'
      },
      {
        // HTML LOADER
        test: /\.html$/,
        loader: 'html-loader'
      }
    ]
    },
    resolve: {
      alias: {
        'assets': Path.resolve(__dirname, 'src/assets'),
        modernizr$: Path.resolve(__dirname, ".modernizrrc")
      }
    }
  };

  if (options.isProduction) {
    webpackConfig.entry = {
      'app': ['./src/js/index'],
      'vendor': ['classie', 'modernizr','cropperjs','wolfy87-eventemitter','./src/js/utils']
    };

    webpackConfig.plugins.push(
      new Webpack.optimize.OccurenceOrderPlugin(),
      new Webpack.optimize.UglifyJsPlugin({
        compressor: {
          warnings: false
        }
      }),
      ExtractSASS
    );

    webpackConfig.module.loaders.push({
      test: /\.scss$/i,
      loader: ExtractSASS.extract(['css', 'sass'])
    });

  } else {
    webpackConfig.plugins.push(
      new Webpack.HotModuleReplacementPlugin()
    );

    webpackConfig.module.loaders.push({
      test: /\.scss$/i,
      loaders: ['style', 'css', 'sass']
    }, {
      test: /\.js$/,
      loader: 'eslint',
      exclude: /node_modules/
    });
    /*,{
      test: /\.(png|jpg)$/,
      loader: 'url-loader?limit=90000'
    }*/

    webpackConfig.devServer = {
      contentBase: './dist',
      hot: true,
      port: options.port,
      inline: true,
      progress: true
    };
  }

  return webpackConfig;

}
