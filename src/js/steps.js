import Modernizr from 'modernizr';
import Classie from 'classie';

import utils from './utils.js';
import EventEmitter from 'wolfy87-eventemitter';
import Cropper from 'cropperjs';

import 'cropperjs/dist/cropper.css';

/*
 * First step, image upload
 */
var StepOne = function(){
  this.el = '#sticker__pick';
  this.domEl = document.querySelector(this.el);

  this.uploadButton = this.domEl.querySelector('#imagepicker__input');
  this.infomsgEl     = this.domEl.querySelector('.infomsg');

  this.activeFileObject = undefined;

  this.errorMessage = () => {
    Classie.remove( this.infomsgEl, 'pulse' );
    void this.infomsgEl.offsetWidth;   // reflow necessary;
    Classie.add( this.infomsgEl , 'pulse' );
  };

  var allowed_mime = ["image/jpg", "image/jpeg"];
  this.isValidFile = (file) => {
    return (allowed_mime.indexOf(file.type) > -1);
  };

  /*
   * load image and check its size
   * returns a promise
   */
  var minimum_size = [600,433];
  this.isImageBigEnough = (local_file) => {
    return new Promise( (resolve, reject) => {
      var img = new Image();
      img.addEventListener('load', () => {
        console.log(img.width, img.height);
        if(img.width < minimum_size[0] ||
           img.height < minimum_size[1]){
          reject();
        } else {
          resolve();
        }
      });
      img.addEventListener('error', (e) => {
        console.error(e);
        reject();
      });
      img.src = local_file;
    });
  };

  this.readLocalFile = (ev) => {
    var file = ev.target.files[0];
    if(! file) return;

    if( ! this.isValidFile(file) ){
      this.errorMessage();
      return;
    }

    // free previous image object (if any)
    if(this.activeFileObject){
      URL.revokeObjectURL(this.activeFileObject);
    }

    this.activeFileObject = URL.createObjectURL( file );
    this.isImageBigEnough(this.activeFileObject).then( () => {
      this.trigger('file_selected', [this.activeFileObject]);
    }).catch( () => {
      this.errorMessage();
    });

  };

  this.uploadButton.addEventListener('change', this.readLocalFile);
  return this;
};
StepOne.prototype = Object.create(EventEmitter.prototype);






/*
 * Second step, image crop & positioning
 */
var StepTwo = function(){
  this.el = '#sticker__frame';
  this.domEl = document.querySelector(this.el);

  this.imageEl = this.domEl.querySelector('#image__framed');
  this.qualityMeterEl = this.domEl.querySelector('.meter');
  this.qualityMeterBar = this.domEl.querySelector('.meter > .bar');

  this.nextButtonEl = this.domEl.querySelector('.actions > .next');
  this.previousButtonEl = this.domEl.querySelector('.actions > .previous');

  // quality meter
  // Check aspectRatio with the following reference
  var reference_quality = [{delta: [0,       0.10],      pct: [101.0,  75.0],  type: 'excellent'},
                           {delta: [0.11,    0.25],      pct: [75.0,   44.0],  type: 'good'},
                           {delta: [0.26,    0.6],       pct: [44.0,   22.0],   type: 'ok'},
                           {delta: [0.6,     3.0],       pct: [22.0,    0.0],   type: 'shit'}];

  // is this correct ?
  // im using the layout as a base reference for the sticker aspect ratio
  var reference_ratio = 162/312.0;
  var previous_quality_type = 'shit';
  this.qualityCheck = ( crop_data ) => {
    var ratio       = crop_data.width / crop_data.height;
    var ratio_delta = Math.abs( ratio - reference_ratio );

    // get nearest quality
    var quality = reference_quality[2];
    for (var ref of reference_quality) {
      if(ratio_delta < ref.delta[1]){
        quality = ref;
        break;
      }
    }

    // console.log("ratio --> ", ratio_delta);
    // console.log("Nearest quality is --> ", quality.type);
    // Now map the delta to percentage and apply to meter-bar
    var pct = utils.map_range( ratio_delta, quality.delta[0], quality.delta[1], quality.pct[0], quality.pct[1]);
    this.qualityMeterBar.style.width = `${pct}%`;

    if(quality.type != previous_quality_type){
      Classie.remove( this.qualityMeterEl , previous_quality_type );
      Classie.add( this.qualityMeterEl, quality.type );
      previous_quality_type = quality.type;
    }
  };

  // set cropper
  var minimum_size = {desktop: [162, 312], mobile: [81,156]};
  this.cropper = new Cropper( this.imageEl , {
    guides: false,
    background: false,
    movable: false,
    zoomable: false,
    minCropBoxWidth: (utils.viewportMobileSize() ? minimum_size.mobile[0] : minimum_size.desktop[0]),
    minCropBoxHeight: (utils.viewportMobileSize() ? minimum_size.mobile[1] : minimum_size.desktop[1]),
    crop: (e)  => {
      this.qualityCheck( this.cropper.getData(true) );
    }
  });

  this.on( 'rebuild', (file_blob) => {
    this.imageEl.src = file_blob;
    this.cropper.clear();
    this.cropper.replace( file_blob );
  });

  this.nextButtonEl.addEventListener( utils.eventClick(), (e) => {
      this.trigger( 'crop_complete', [this.cropper.getCroppedCanvas(), this.cropper.getData(true)] );
  });
  this.previousButtonEl.addEventListener( utils.eventClick(), (e) => {
      this.trigger( 'goback' );
  });

  return this;
};
StepTwo.prototype = Object.create(EventEmitter.prototype);


/*
 * Step three, image preview
 */
var StepThree = function(){
  this.el = '#sticker__confirm';
  this.domEl = document.querySelector(this.el);

  this.stickerEl      = this.domEl.querySelector('.preview > .sticker');
  this.finalImageEl   = this.domEl.querySelector('.final-image');

  this.submitButtonEl = this.domEl.querySelector('.actions > .next');
  this.previousButtonEl = this.domEl.querySelector( '.actions > .previous');

  this.signCheckEl = this.domEl.querySelector( '#termssignature' );

  this.drawImages       = {"mask": undefined, "shadow": undefined};
  this.drawImagesURI    = {"mask":   require("assets/images/mascara2-without-shadow.png"),
                           "shadow": require("assets/images/mascara2-shadow.png")};

  /*
   * Load images
   */
  var loadedImages = 0;
  this.loadImages = () => {
    for(var key in this.drawImagesURI){
      var uri = this.drawImagesURI[key];

      var img = new Image();
      img.src = uri;
      img.onload = () => { loadedImages++; };

      this.drawImages[key] = img;
    }
    return;
  };

  // preload it plz
  this.loadImages();
  this.isImagesLoaded = () => { return (loadedImages == (Object.keys(this.drawImages).length)); };

  /*
   * draw image with mask and Shadow
   */
  this.drawUserImageMasked = (canvas, image ) => {

    if( ! this.isImagesLoaded() ){
      console.log("Waiting for mask images to be loaded ... plz wait ");
    } else {
      var ctx = canvas.getContext("2d");

      // clear canvas
      ctx.clearRect(0, 0, canvas.width, canvas.height );
      ctx.save();

      // draw mask
      ctx.drawImage( this.drawImages['mask'], 0, 0, canvas.width, canvas.height);

      // draw image only on the existing pixels (masked)
      ctx.globalCompositeOperation = "source-in";
      ctx.drawImage( image , 0 , 0);

      // Draw shadow on top
      ctx.globalCompositeOperation = "source-atop";
      ctx.drawImage( this.drawImages['shadow'] , 0 , 0, canvas.width, canvas.height);

      ctx.restore();
    }
  };

  /*
   * Draw the sticker on the final image
   */
  this.drawSticker = (canvas) => {
    this.copyCanvasToImage( canvas , 'image/png' ).then( (final_img) => {

      final_img.id = 'final-user-image';
      Classie.add( final_img , 'opacityIn' );

      var currentImg = this.finalImageEl.querySelector('#final-user-image');
      if(currentImg == null){
        this.finalImageEl.appendChild( final_img  );
      } else {
        currentImg.replaceWith( final_img );
      }

    });
  };


  /*
   * Render sticker picture on top of

  /*
   * Create our drawing canvas to the DOM (if necessary)
   */
  this.insertDrawingCanvas = ( width, height ) => {
    var canvas = this.stickerEl.querySelector('canvas');
    if(canvas == null){
      var canvas = document.createElement('canvas');
      this.stickerEl.appendChild( canvas  );
    }

    // Update coordinates
    canvas.width  = width;
    canvas.height = height;
    canvas.style.width  = '100%';
    canvas.style.height = '100%';

    return canvas;
  };

  /*
   * Transform a canvas to a img tag
   */
  this.copyCanvasToImage = ( canvas , image_type) => {
    var image_type = (image_type != undefined ? image_type : 'image/png');
    return new Promise( (resolve, reject) => {

      var img = document.createElement('img');

      // Copy the cropped image to a img
      canvas.toBlob( (blob) => {
        var url = URL.createObjectURL(blob);

        img.onload = () => {
          URL.revokeObjectURL(url);
          resolve(img);
        };
        img.onerror = () => {   reject(); };
        img.src = url;
      }, image_type, 1.0);
    });
  };

  /*
   * Render image with crop bounds and mask
   */
  this.on( 'render', (crop_canvas, crop_data) => {

    var drawingCanvas = this.insertDrawingCanvas( crop_data.width, crop_data.height );
    this.copyCanvasToImage( crop_canvas , 'image/jpg' ).then( (cropped_img) => {
      console.log("cropped_img --> ", cropped_img.src);

      // Cut user picture accordingly
      this.drawUserImageMasked( drawingCanvas, cropped_img );

      // Draw final image
      this.drawSticker( drawingCanvas );
    });


  });



  this.submitButtonEl.addEventListener( utils.eventClick(), (e) => {
      if(! this.signCheckEl.checked ){
        /* Trigger animation */
        Classie.remove( this.signCheckEl.parentNode, 'pulse' );
        void this.signCheckEl.parentNode.offsetWidth;   // reflow necessary;
        Classie.add( this.signCheckEl.parentNode , 'pulse' );
      } else {
        this.trigger( 'submit' );
      }
  });
  this.previousButtonEl.addEventListener( utils.eventClick(), (e) => {
      this.trigger( 'goback' );
  });


  return this;
};
StepThree.prototype = Object.create(EventEmitter.prototype);


module.exports = {
  StepOne,
  StepTwo,
  StepThree
};
