import Modernizr from 'modernizr';
import Classie from 'classie';

import utils from './utils.js';
import EventEmitter from 'wolfy87-eventemitter';

/*
 * Navigate between tabs.
 *
 * options:
 *      container: ID of container elements
 *      wrappper:  query of wrapped tabs
 */
function TabSlider( options ){

  this.container         = document.querySelector( options.container || '#sticker-container');
  this.wrapper           = this.container.querySelector( options.wrapper || 'div.sticker__wrap');
  this.tabIndexes        = this.container.querySelector( options.tabIndexes || '.tab-indexes');

  this.links     = Array.prototype.slice.call( this.tabIndexes.querySelectorAll('.tab-link') );
  this.tabs      = Array.prototype.slice.call( this.wrapper.querySelectorAll('.tab'));

  if(options.defaultTab){
    this.activeTab  = (options.hasOwnProperty('defaultTab') ? options.defaultTab : this.tabs[0].getAttribute('id'));  //this.container.querySelector( options.defaultTab );
    this.activeLink = `a[data-target="${options.defaultTab}"]`; //this.container.querySelector(`a[data-target="${options.defaultTab}"]`);
  }else{
    this.activeTab = this.tabs[0].getAttribute('id');
    this.activeLink = this.links[0].getAttribute('data-target');
  }
  this.previousTab = false;
  let eventtype = utils.eventClick();

  // Setup link listeners
  this.links.forEach( (el, i) => {
    el.addEventListener( eventtype, (ev) => {
      ev.preventDefault();

      var tab_target = el.getAttribute('data-target');
      if(tab_target){
        this.trigger('navigate', [tab_target] );
      } else {
        return false;
      }
    });
  });

  // clean active tabs
  var cleanTabs = () => {
    this.links.forEach( (el,i) => {
      Classie.remove( el, 'link--active' );
    });
    this.tabs.forEach( (el,i) => {
      Classie.remove( el, 'tab--active' );
      Classie.add( el, 'tab--hidden');
    });
  };


  // navigate to the #id_el tab
  this.navigate = function( id_el ){
    console.log("navigate to --> ", id_el);

    // hidde previous tab
    var tab = this.container.querySelector(this.activeTab);
    var link = this.container.querySelector( this.activeLink);
    Classie.remove( tab , 'tab--active' );
    Classie.add(    tab, 'tab--hidden' );
    Classie.remove( link, 'link--active' );
    Classie.remove( this.tabIndexes, 'tab-active--'+this.activeTab.substr(1) );

    // update pointers
    this.previousTab = this.activeTab;
    this.activeTab   = id_el;
    this.activeLink  = `a[data-target="${id_el}"]`;

    // show current tab
    var tab_target = this.container.querySelector( this.activeTab );
    var link_target = this.container.querySelector(this.activeLink);
    Classie.remove( tab_target, 'tab--hidden');
    Classie.add( tab_target,  'tab--active');
    Classie.add( link_target, 'link--active');
    Classie.add( this.tabIndexes, 'tab-active--'+this.activeTab.substr(1) );

  };

  cleanTabs();
  this.navigate( this.activeTab );

  return this;
}
TabSlider.prototype = Object.create(EventEmitter.prototype);

module.exports = TabSlider;
