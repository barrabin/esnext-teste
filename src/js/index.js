'use strict';

if (module.hot) {
  module.hot.accept();
}
import 'babel-polyfill';


import Modernizr from 'modernizr';
import Classie from 'classie';

import utils from './utils.js';
import TabSlider from './tabSlider.js';

import { StepOne, StepTwo, StepThree } from './steps.js';

import '../styles/index.scss';


/*
 * The sticker editor glues everything cohesely in a single place
 */
function StickerEditor( options ) {

  this.stickerTabs = new TabSlider({container: '#sticker-container',
                                    wrapper: 'div.sticker__wrap',
                                    defaultTab: '#sticker__pick'});

  this.steps      = [new StepOne(), new StepTwo(), new StepThree()];
  this.activeStep =  this.steps[0].el;

  this.originalImage = undefined;
  this.imageCropData = {};
  this.cropComplete  = false;

  /*
   * When file selected, move to next tab
   */
  this.steps[0].on( 'file_selected', (file_blob) => {
    this.originalImage = file_blob;
    this.steps[1].trigger('enter');
    this.steps[1].trigger('rebuild', [file_blob]);

    // reset crop state
    this.cropComplete  = false;

    this.activeStep = this.steps[1].el;
    this.stickerTabs.navigate( this.activeStep );
  });

  // Crop complete, move to next tab
  this.steps[1].on( 'crop_complete', (crop_canvas, crop_data) => {
    this.imageCropData = crop_data;
    this.steps[2].trigger('enter');
    this.steps[2].trigger('render', [crop_canvas, this.imageCropData]);

    this.cropComplete  = true;

    this.activeStep = this.steps[2].el;
    this.stickerTabs.navigate( this.activeStep );
  });

  // Buying confirmed, ship to the server!
  this.steps[2].on('submit', () => {
    console.info("Thank you for buying with us!");

    // todo.
    // upload image
  });


  // enable back button
  this.steps.forEach( (step) => {
    step.on( 'goback', () => {
      var previous_step = '#sticker__pick';

      if(this.activeStep == '#sticker__frame') previous_step = '#sticker__pick';
      else if(this.activeStep == '#sticker__confirm') previous_step ='#sticker__frame';

      this.stickerTabs.navigate( previous_step );
      this.activeStep = previous_step;
    });
  });


  /*
   * Disable jumping ahead of steps
   */
  this.stickerTabs.on('navigate', (tab_target) => {
    var allow_click = false;

    if((tab_target === this.steps[0].el) ||
       (tab_target === this.steps[1].el && this.originalImage != undefined) ||
       (tab_target === this.steps[2].el && this.cropComplete )){
      allow_click = true;
    }

    if(allow_click){
      this.stickerTabs.navigate(tab_target);
    }
  });


  /*
   * Enable windows/osx guide Modals
   */
  var current_os = 'modal--windows';
  if (navigator.appVersion.indexOf("Mac")!=-1) current_os = 'modal--osx';

  function toggleOS(new_os){
    var guide_modal = document.querySelector('#imgquality-modal .content');
    Classie.remove(guide_modal, current_os);
    Classie.add(guide_modal, new_os);
    current_os = new_os;
  }
  toggleOS(current_os);

  document.querySelector('#guide-osx-toggler').addEventListener( utils.eventClick(), (ev) => {
    toggleOS( 'modal--osx' );
    ev.preventDefault();
  });
  document.querySelector('#guide-windows-toggler').addEventListener( utils.eventClick(), (ev) => {
    toggleOS( 'modal--windows' );
    ev.preventDefault();
  });


  /*
   * Accept terms&conditions
   */
  document.querySelector('#terms-modal-acceptbtn').addEventListener( utils.eventClick(), (ev) => {
    document.querySelector("#termssignature").checked = true;
  });


  /*
   * Submenu trigger
   */
  function submenu_toggle( m ){
    return (ev) => {
      // allow events to bubble down when submenu-active
      var child_or_parent = ev.target.parentNode;
      if(child_or_parent != m &&
         Classie.has(m, 'has-submenu--active')){
        return;
      }

      Classie.toggle(m,'has-submenu--active');
      ev.preventDefault();
    };
  }

  function submenu(){
    var menus = document.querySelectorAll('#pagesnav > ul > li');
    for (var m of menus) {
      var has_submenu_children = m.querySelector('.submenu');
      if(has_submenu_children){
        Classie.add(m,'has-submenu');
        m.addEventListener( utils.eventClick(), submenu_toggle(m) );
      }
    }
  }
  submenu();

  return this;
}




/*
 * Globals and main()
 */
window.stickerEditor = undefined;
function init() {
  window.stickerEditor = new StickerEditor({});
}
init();
